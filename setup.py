#! /usr/bin/env python3

"""
RemoteH - RemoteH is to support others

RemoteH is a utility to facilitate the connection of VNC

Based on Gitso, from Aaron Gerber and Derek Buranen, @copyright: 2008 - 2010

RemoteH is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RemoteH is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RemoteH.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import re
from glob import glob
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install



class PostInstallCommand(install):
	"""Post-installation for installation mode."""
	def run(self):
		print("\n############################\nPost install\n#################################\n")
		install.run(self)
		if platform == 'windows':

			try:
				os.system("rcedit {:s} --set-icon {:s}".format(os.path.join(self.root,"SCRIPTS","RemoteH.exe"),os.path.join(os.getcwd(),"src","RemoteH","icons","remoteh.ico")))
			except:
				pass

OPTIONS = {}

if sys.platform == 'darwin':
	platform = 'mac'
elif re.match('(?:open|free|net)bsd|linux',sys.platform):
	platform = 'unix'
elif sys.platform.startswith('win'):
	platform = 'windows'
else:
	platform = 'unknown'

has_dep = False

if platform=='windows2':
	import py2exe
	OPTIONS = {'argv_emulation': True}


def get_data_files():

	global platform
	global has_dep

	data_files = []

	if platform == 'unix':
		data_files.append((os.path.join('.','share','icons','hicolor','scalable','apps'),['data/icons/remoteh.svg']))
		data_files.append((os.path.join('.','share','applications'),['data/remoteh.desktop']))
	if platform == "windows":
		data_files.append((os.path.join('.'),['data/icons/remoteh.ico']))
		data_files.append((os.path.join('.'),['data/icons/remoteh.png']))
	else:
		data_files.append((os.path.join('.','share','remoteh'),['data/icons/remoteh.png']))
	data_files.append((os.path.join('.','share','doc','remoteh'),['COPYING']))

	try:
		for lang_name in [f for f in os.listdir('locale')]:
			mofile = os.path.join('locale', lang_name,'LC_MESSAGES','remoteh.mo')
			# translations must be always in /usr/share because Gtk.builder only search there. If someone knows how to fix this...
			if platform == 'windows':
				target = os.path.join('.','locale', lang_name, 'LC_MESSAGES') # share/locale/fr/LC_MESSAGES/
			else:
				target = os.path.join('.','share', 'locale', lang_name, 'LC_MESSAGES') # share/locale/fr/LC_MESSAGES/
			data_files.append((target, [mofile]))
	except:
		pass
	return data_files



#here = os.path.abspath(os.path.dirname(__file__))

params_setup = {}

params_setup['name'] = 'remoteh'
params_setup['version'] = '0.4.1'
params_setup['description']='RemoteH is to support others'
params_setup['long_description']="A program to simplify using reverse VNC"
params_setup['license']='GPLv3'
params_setup['packages']=['RemoteH']
params_setup['package_dir'] = {"RemoteH" : "src/RemoteH"}
params_setup['url'] = 'http://www.rastersoft.com/programas/remoteh.html'
params_setup['author'] = 'Sergio Costas-Rodriguez (Raster Software Vigo)'
params_setup['author_email'] = 'rastersoft@gmail.com'

params_setup['cmdclass'] = {'install': PostInstallCommand}

params_setup['entry_points'] = {'gui_scripts': ['remoteh = RemoteH.remoteh:main']}
package_data = ['icons/remoteh.png']

if platform == 'windows':
	params_setup['console'] = ['remoteh']
	package_data += ['winbinaries/vncviewer.exe', 'winbinaries/winvnc.exe', 'winbinaries/UltraVNC.ini']
package_data += [c.replace("src/RemoteH/","") for c in glob("src/RemoteH/locale/**/*.mo",recursive=True)]
#else:

params_setup['package_data'] = {'RemoteH': package_data}
setup(**params_setup)
