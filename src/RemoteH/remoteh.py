#! /usr/bin/env python3

"""
RemoteH - RemoteH is to support others

RemoteH is a utility to facilitate the connection of VNC

Based on Gitso, from Aaron Gerber and Derek Buranen, @copyright: 2008 - 2010

RemoteH is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RemoteH is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RemoteH.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import platform

import tkinter
import os
import gettext
import locale
import pkg_resources

import RemoteH.ConnectionWindow
import RemoteH.ArgsParser

def main():

	if sys.platform.startswith('win'):
		if os.getenv('LANG') is None:
			lang, enc = locale.getdefaultlocale()
			os.environ['LANG'] = lang

	args = RemoteH.ArgsParser.ArgsParser()
	locale.setlocale(locale.LC_ALL, '')
	gettext.bindtextdomain('remoteh',pkg_resources.resource_filename('RemoteH','locale'))
	gettext.textdomain('remoteh')

	w = RemoteH.ConnectionWindow.ConnectionWindow(args.GetPaths())
	w.run()

def wininstaller():

	with open("c:\\Users\\raster\\datos.txt","w") as datos:
		datos.write("Path: {:s}\n".format(sys.prefix))

	if sys.argv[1] == '-install':

		# Get paths to the desktop and start menu
		desktop_path = get_special_folder_path("CSIDL_COMMON_DESKTOPDIRECTORY")
		startmenu_path = get_special_folder_path("CSIDL_COMMON_STARTMENU")

		# Create shortcuts.
		for path in [desktop_path, startmenu_path]:
			create_shortcut("remoteh","RemoteH is to help others","RemoteH")
