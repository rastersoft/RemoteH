��    +      t  ;   �      �     �     �     �  
   �     �     �  	   �     �     �       %   !  2   G  /   z  >   �  !   �          %     *     /     8     E     J     O  7   g     �     �     �     �     �     �  �   �  %   �     �     �     �     �     �  @   �  -   8  D   f     �     �  �  �  	   i	     s	     �	  
   �	     �	     �	  	   �	     �	     �	     �	  )   �	  =   
  3   B
  6   v
  3   �
  0   �
                     ,     8  	   >      H  E   i     �  	   �     �     �     �  !   �  �     '   �  %   �     �     �            E   '  8   m  >   �     �     �               +                    "                    %                           	          )      &          !      
                $       (                                     '             *      #    About About RemoteH Close Connected. Connecting... Copy Copyright Cut Edit Enter/Select Support Address Error: '{:s}' is not a valid argument Error: '{:s}' is not a valid host with '--connect' Error: '{:s}' is not a valid list with '--list' Error: --connect and --listen can not be used at the same time Error: No IP or domain name given Error: No List file given Exit File Get Help Give Support Help Idle Invalid Support Address Launched WinVNC.exe, waiting to run -connect command... License No error Paste Platform not detected Processes.KillPID(%s) RemoteH is to Support Others RemoteH {:s}  -- Copyright 2016 Sergio Costas Rodriguez;
based on Gitso, from Aaron Gerber and Derek Buranen @copyright: 2008 - 2010. RemoteHThread.run(pid: %s) running... Running in 'Development Mode' Server running. Start Starting Server... Stop The operation was refused.  NAT-PMP may be turned off on gateway The protocol version specified is unsupported There was a network failure.  The gateway may not have an IP address Use low colors Version {:s} Project-Id-Version: 0.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-01 16:35+0200
PO-Revision-Date: 2016-08-01 16:39+0200
Last-Translator: Sergio Costas <rastersoft@gmail.com>
Language-Team: Español; Castellano <rastersoft@gmail.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 Acerca de Acerca de RemoteH Cerrar Conectado. Conectando... Copiar Copyright Cortar Editar Dirección de soporte Error: '{:s}' no es un parámetro válido Error: '{:s}' no es un host válido para usar con '--connect' Error: '{:s}' no es una lista válida para '--list' Error: --connect y --listen no se pueden usar a la vez Error: no se ha indicado un dominio o dirección IP Error: no se ha indicado un fichero con la lista Salir Fichero Pedir ayuda Dar soporte Ayuda En espera Dirección de soporte no válida Lanzado WinVNC.exe, esperando a que se ejecute el comando -connect... Licencia Sin error Pegar Plataforma desconocida Processes.KillPID(%s) RemoteH, para dar soporte a otros RemoteH {:s}  -- Copyright 2016 Sergio Costas Rodríguez;
basado en Gitso, de Aaron Gerber y Derek Buranen @copyright: 2008 - 2010 RemoteHThread.run(pid: %s) en marcha... Ejecutándose en 'Modo desarrollador' Servidor en marcha. Comenzar Arrancando el servidor... Detener Operación rechazada. NAT-PMP podría estar desactivado en el gateway La versión de protocolo especificada no está soportada Error en la red. El gateway podría no tener una dirección IP Usar menos colores Versión {%s} 