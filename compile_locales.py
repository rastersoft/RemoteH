#! /usr/bin/env python3

import os

def compile_translations():

	basepath = 'src/RemoteH/locale'
	try:
		if (0 == os.system("msgfmt -h > /dev/null")) and os.path.exists('po'):
			os.system("rm -rf {:s}".format(basepath))
			for pofile in [f for f in os.listdir('po') if f.endswith('.po')]:
				pofile = os.path.join('po', pofile)

				lang = os.path.basename(pofile)[:-3] # len('.po') == 3
				modir = os.path.join(basepath, lang, 'LC_MESSAGES') # e.g. locale/fr/LC_MESSAGES/
				mofile = os.path.join(modir, 'remoteh.mo') # e.g. locale/fr/LC_MESSAGES/devede_ng.mo

				# create an architecture for these locales
				if not os.path.isdir(modir):
					os.makedirs(modir)

				if not os.path.isfile(mofile) or (has_dep and dep_util.newer(pofile, mofile)):
					# msgfmt.make(pofile, mofile)
					os.system("msgfmt \"" + pofile + "\" -o \"" + mofile + "\"")
					os.system("git add {:s}".format(mofile))
	except:
		pass

compile_translations()
